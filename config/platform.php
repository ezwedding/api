<?php

return [

    /*
    |--------------------------------------------------------------------------
    | 平台使用的參數
    |--------------------------------------------------------------------------
    |
    | 由此載入 env 設定值
    |
    */
    'seeder_user_name' => env('SEEDER_USER_NAME', 'user'),
    'seeder_user_email' => env('SEEDER_USER_EMAIL', 'user@localhost.local'),
    'seeder_user_password' => env('SEEDER_USER_PASSWORD', '123456'),
];
