<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*
|--------------------------------------------------------------------------
| EZ-Wedding API Routes
|--------------------------------------------------------------------------
|
| 說明請寫在這裡
|
*/
Route::group([
    'namespace' => 'App\Http\Controllers\Api',
    'as' => 'admin.api.'
], function () {

    // ----------------------------
    //   公開 API
    // ----------------------------
    Route::get('say-hello-world', function () {
        echo json_encode(['message' => 'Hello World!!']);
    });

    // ----------------------------
    //   需要登入才可使用 API
    // ----------------------------
    Route::group([
        'middleware' => [
            'auth:api' // for passport token validation
        ]
    ], function () {
        Route::get('passport-token-test', function () {
            echo json_encode(['message' => 'Looks Good!!']);
        });
    });
});