# Insomnia 導入 ez-wedding-api_yyyy-mm-dd.json 指引

請注意本 ```ez-wedding-api_yyyy-mm-dd.json``` 僅能匯入 [Insomnia](https://insomnia.rest/download/#mac) 這個應用程式，請先抓下來安裝。

## 說明

本機開發時，環境設定中需修改相關參數

- ```url``` 專案對應的 domain，例如 http://ez-wedding.local
- ```client_id``` 與 ```client_secret``` [註1]
- ```access_token``` 授權 token，取得方式請參考 [註1] [註2]

P.S. 環境變數會依照字母自動排序，不會影響訪問任何 API 上的使用。 

## 常見問題

### 如何使用環境變數

每個 API 請求任務都會帶有一些 ```Header```，在填參數的介面的子頁籤可以找到，其中對應系統商時會帶：
```
Content-Type: application/json
X-Requested-With: XMLHttpRequest
Authorization: Bearer {{ access_token  }}
```
這邊的 ```{{ access_token  }}``` 就是引入的環境變數，所以你可以選擇 ```本機測試 localhost``` 或其他環境來快速切換變數

## 註解

1. 當你在 console cd 到專案底下執行 ```php artisan passport:install```，會在對應 .env 中設定的資料庫（DB_HOST）表 ```oauth_clients``` 產生相關 oauth 資料，```client_id``` 2 表示對應 id 為 2 的 row，其欄位 ```secret``` 就是 API 工具的環境變數 ```client_secret```，將數值填到環境變數中，才可順利取得系統授權 token。
2. 點選「取得授權」API，填入對應的 user email 與 password (console 中 cd 到專案底下執行 ```php artisan db:seed --class=CreateUserSeeder``` 產生的帳號），這組帳號、密碼初始值需要在 .env 中的 ```SEEDER_USER_EMAIL```、```SEEDER_USER_PASSWORD``` 設定。