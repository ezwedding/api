<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = config('platform.seeder_user_name');
        $user->email = config('platform.seeder_user_email');
        $user->password = config('platform.seeder_user_password');
        $user->save();
    }
}
