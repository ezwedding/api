## Install

下載 repo

    $ git clone git@bitbucket.org:ezwedding/api.git # 須先於 bitbucket 設置 ssh-key，也可透過 https 方式 clone
    $ cd api/
    $ git checkout dev-master # 請注意，開發時請再另開新的分支
    
專案若無法順利安裝請發 issue。若無 ```composer``` 請先[安裝](https://getcomposer.org/download/) ```composer```。

請逐步執行以下指令初始專案：

    $ chmod -R 777 storage/
    $ chmod -R 777 bootstrap/cache
    $ chmod 777 bootstrap/
    $ cp .env.example .env 
    $ vi .env # 填入相關的設定 (請設定 DB 連線資訊) 
    $ composer install
    $ php artisan key:generate # 必須先 composer install
    $ php artisan migrate # 必須先設定 .env 
    $ php artisan passport:install # 必須先 composer install & 設定 .env
    
## SEEDER

設定 .env 種子參數部分

    # ----------------
    #   SEEDER PARAMS
    # ----------------
    SEEDER_USER_NAME=user_name
    SEEDER_USER_EMAIL=user_name@example.local
    SEEDER_USER_PASSWORD=q123456

並執行指令，可產生一個預設使用者資料

    $ php artisan db:seed --class=CreateUserSeeder

備註：需要測試 API 時，須先取得授權 token，取得方式會使用到 email 與 password 這兩個設定值

# Insomnia 

API 工具請參考 ```/api-doc```