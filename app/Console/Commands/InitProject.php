<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Finder\Exception\AccessDeniedException;

class InitProject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:project';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '初始化專案';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        shell_exec('chmod -R 777 ' . storage_path());
        shell_exec('chmod -R 777 ' . app('path.bootstrap') . '/cache');
        shell_exec('chmod -R 777 ' . app('path.bootstrap'));
        $this->info('permissions setting successfully');

        shell_exec('cp ' . dirname(app('path')) . '/.env.example ' . dirname(app('path')) . '.env');

        $DB_DATABASE = $this->ask('請輸入 DATABASE 連線 NAME, 例如 ez-wedding');
        $DB_USERNAME = $this->ask('請輸入 DATABASE 連線 USERNAME, 例如 root');
        $DB_PASSWORD = $this->ask('請輸入 DATABASE 連線 PASSWORD, 例如 1234');

        $this->setEnvironmentValue('DB_DATABASE', $DB_DATABASE);
        $this->setEnvironmentValue('DB_USERNAME', $DB_USERNAME);
        $this->setEnvironmentValue('DB_PASSWORD', $DB_PASSWORD);
        $this->info('environment value setting successfully');

        shell_exec('composer install');

        Artisan::call('key:generate');
        Artisan::call('migrate');
        Artisan::call('passport:install');
        Artisan::call('db:seeder', ['--class' => 'CreateUserSeeder']);
    }

    /**
     * 設定 .env，僅限開發環境使用
     *
     * @param $envKey
     * @param $envValue
     */
    private function setEnvironmentValue($envKey, $envValue)
    {
        if (config('APP_ENV') === 'local') {
            $envFile = app()->environmentFilePath();
            $str = file_get_contents($envFile);
            $oldValue = strtok($str, "{$envKey}=");
            $str = str_replace("{$envKey}={$oldValue}", "{$envKey}={$envValue}\n", $str);
            $fp = fopen($envFile, 'w');
            fwrite($fp, $str);
            fclose($fp);
        } else {
            throw new AccessDeniedException();
        }
    }
}
