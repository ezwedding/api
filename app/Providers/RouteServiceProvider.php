<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Passport::routes();

        if (config('app.env') !== 'production') {
            Passport::tokensExpireIn(now()->addDays(30));
            Passport::refreshTokensExpireIn(now()->addDays(60));
        } else {
            Passport::tokensExpireIn(now()->addHours(2));
            Passport::refreshTokensExpireIn(now()->addHours(3));
        }
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $routeMiddleware = [
            'web'
        ];

        Route::namespace($this->namespace)
            ->middleware($routeMiddleware)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        $routeMiddleware = [
            'api'
        ];

        Route::prefix('api')
            ->middleware($routeMiddleware)
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
